{ config, lib, name, ... }:

let
  inherit (lib) mkOption types;
in {
  options = {
    port = mkOption {
      type = types.port;
      default = 5182;
      description = "Port for the ${name} netbird interface.";
    };

    adminURL = mkOption {
      type = types.str;
      default = "https://app.netbird.io:443";
      description = "Admin panel URL";
    };

    daemonAddr = mkOption {
      type = types.str;
      default = "unix:///var/run/netbird-client/${name}.sock";
      description = "Daemon service address to serve CLI requests";
    };

    managementURL = mkOption {
      type = types.str;
      default = "https://api.netbird.io:443";
      description = "Managment service URL";
    };

    setupKey = mkOption {
      type = types.str;
      default = "";
      description = ''
        Setup key for Netbird login.  This can be safe to be public since setup keys can be
        configured to one-time use.  After being consumed, the key is no longer valid and
        the Wireguard public key has been shared with the Netbird server.

        DO NOT USE AN UNLIMITED USE SETUP KEY FOR THIS VALUE!
      '';
    };
  };
}