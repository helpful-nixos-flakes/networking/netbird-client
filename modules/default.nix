{ config, lib, pkgs, utils, ... }:

let
  inherit (lib)
    attrNames
    getExe
    mapAttrs'
    mkEnableOption
    mkIf
    mkMerge
    mkOption
    mkPackageOption
    nameValuePair
    optionals
    types;

  inherit (utils) escapeSystemdExecArgs;
  
  cfg = config.services.netbird-client;
in {
  options.services.netbird-client = {
    enable = mkEnableOption "Netbird daemon";
    package = mkPackageOption pkgs "netbird" { };

    logLevel = mkOption {
      type = types.enum [ "debug" "info" "warn" "error" ];
      default = "info";
      description = "Sets the Netbird log level.";
    };

    externalIPMap = mkOption {
      type = types.listOf types.str;
      default = [];
      example = [ "12.34.56.200" "12.34.56.78/10.0.0.1" "12.34.56.80/eth1" ];
      description = "";
    };

    tunnels = mkOption {
      type = types.attrsOf (types.submodule (import ./tunnel.nix));
      default = { wt0 = { }; };
      description = "Attribute set of Netbird tunnels.  Each tunnel will spawn a separate service.";
    };

    extraInterfaceBlacklist = mkOption {
      type = types.listOf types.str;
      default = [];
      example = [ "mywg" "otheriface" ];
      description = "Interfaces for Netbird to ignore for listening.  Defaults are enough for most default configurations.";
    };

    hostName = mkOption {
      type = types.str;
      default = config.networking.hostName;
      example = "myhost";
      description = "Custom hostname for use in Netbird.  Defaults to the machine's hostname.";
    };

    customDNSResolver = mkOption {
      type = types.str;
      default = "";
      example = "1.2.3.4:53";
      description = "Custom DNS resolver for host, overriding the Netbird DNS resolution.";
    };

    allowSSH = mkOption {
      type = types.bool;
      default = false;
      description = "Whether the client allows being configured for SSH connections.";
    };
  };

  config = mkIf (cfg.tunnels != { }) {
    environment.systemPackages = [ cfg.package ];

    networking.dhcpcd.denyInterfaces = attrNames cfg.tunnels;

    systemd.services = mapAttrs' (name: tunCfg: nameValuePair 
      "netbird-client-${name}"
      {
        description = "A WireGuard-based mesh network that connects your devices into a single private network";
        documentation = [ "https://netbird.io/docs/" ];

        after = [ "network.target" ];
        wantedBy = [ "multi-user.target" ];

        environment = {
          # This seems to be the only way to get Netbird to use iptables.  The application's
          # autodetect always decided to use nftables.
          # TODO firewall system configuration check
          NB_SKIP_NFTABLES_CHECK = "true";
        };

        path = with pkgs; [ iptables openresolv ];

        serviceConfig = {
          ExecStart = escapeSystemdExecArgs ([
            "${cfg.package}/bin/netbird"
            "service"
            "run"
            "--config"
            "/var/lib/netbird-client/${name}/config.json"
            "--daemon-addr"
            tunCfg.daemonAddr
            "--log-file"
            "console"
            "--log-level"
            cfg.logLevel
          ]);

          Restart = "always";
          RuntimeDirectory = "netbird-client/${name}";
          StateDirectory = "netbird-client/${name}";
          StateDirectoryMode = "0700";
          WorkingDirectory = "/var/lib/netbird-client/${name}";
        };

        postStart = escapeSystemdExecArgs ([
          "${cfg.package}/bin/netbird"
          "up"
          "--daemon-addr"
          tunCfg.daemonAddr
          "--admin-url"
          tunCfg.adminURL
          "--management-url"
          tunCfg.managementURL
          "--hostname"
          cfg.hostName
          "--interface-name"
          name
          "--wireguard-port"
          (builtins.toString tunCfg.port)
        ] ++ (optionals (tunCfg.setupKey != "") [ "--setup-key" tunCfg.setupKey ])
          ++ (optionals (cfg.externalIPMap != []) [ "--external-ip-map" (lib.concatStringsSep "," cfg.externalIPMap) ])
          ++ (optionals (cfg.extraInterfaceBlacklist != []) [ "--extra-iface-blacklist" (lib.concatStringsSep "," cfg.extraInterfaceBlacklist) ])
          ++ (optionals (cfg.customDNSResolver != "") [ "--dns-resolver-address" cfg.customDNSResolver ])
          ++ (optionals cfg.allowSSH [ "--allow-server-ssh" ]));

        unitConfig = {
          StartLimitInterval = 5;
          StartLimitBurst = 10;
        };

        stopIfChanged = false;
      })
      cfg.tunnels;
  };
}